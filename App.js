import 'react-native-gesture-handler';
import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { Navigator } from './src/utils/Navigator';



export default class Welcome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: 'en',
      isLoading: true,
    };
  }
  //Render loading content 
  render() {
    return (
      <View style={styles.container} >
        <Navigator />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
  }
});






