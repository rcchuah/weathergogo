/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */


import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import { ThemeTextSmall, ThemeTextMedium, ThemeTextLarge, ThemeTextExtraLarge, ThemeButton, ThemeTextbox } from '../components/ThemeComponents';
import Color from '../utils/Color';

export default class Welcome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: 'Singapore',
    };
  }
  //Render loading content 
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.rowContainer}>
          <ThemeTextExtraLarge title='Welcome' />
        </View>
        <View style={styles.rowContainer}>
          <ThemeTextMedium title='Your Location ' />
          <ThemeTextbox title={this.state.city} onChangeText={text => this.setState({city:text})} />
        </View>
        <View style={styles.buttonContainer}>
          <ThemeButton title='Get Weather' style={styles.button}  onPress={() => this.props.navigation.navigate('Home', {city:this.state.city})} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    backgroundColor: Color.white,
  },
  rowContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  button:{
    flex:1,
  },
  buttonContainer:{
    flexDirection:'row',
    marginHorizontal:'15%',
  },
});
