/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */


import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  FlatList,
  Alert,
  Modal,
} from 'react-native';
import { ThemeTextSmall, ThemeTextMedium, ThemeTextLarge, ThemeTextExtraLarge, ThemeButton, ThemeModal } from '../components/ThemeComponents';
import Color from '../utils/Color';
import { GetWeatherByCity } from '../api/api';
import moment from "moment";
import Icon from 'react-native-vector-icons/FontAwesome';


export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      city: '',
      dailyData: [],
      hourlyData: [],
      showModal: false,
      isLoading: true,
      detailData: [],
      titleDate: '',
      titleTemp: '',
      titleWeather: '',
    };
  }

  componentDidMount() {
    this.getWeatherByCity();
  }

  showDetail = (id) => {
    this.setState({
      showModal: true,
      detailData: this.state.dailyData[id].timeSlot
    });
  }

  convertUpperCase(s1) {
    //Convert first letter to upper case
    return s1[0].toUpperCase() + s1.slice(1);
  }

  getWeatherByCity = () => {
    //Retrieve city name from navigation
    const { city } = this.props.route.params;
    this.setState({ city: city });

    GetWeatherByCity(city)
      .then((res) => {
        const data = res.list;
        var dailyData = [];

        //Navigate back to welcome screen if no data. 
        if (!data) {
          Alert.alert(
            'Ops',
            'Invalid city name',
            [{ text: 'OK', onPress: () => { this.props.navigation.navigate('Welcome') } }],
          );
          return;
        }

        //Push first data 
        var id = 0;
        var slotNumber = 0;
        var maxTemp = data[0].main.temp_min;
        var minTemp = data[0].main.temp_max;
        dailyData.push({
          id: id,
          date: moment(data[0].dt_txt).format('l'),
          min: data[0].main.temp_min,
          max: data[0].main.temp_max,
          timeSlot: [
            {
              time: moment(data[0].dt_txt).format('LT'),
              min: data[0].main.temp_min,
              max: data[0].main.temp_max,
              weather: this.convertUpperCase(data[0].weather[0].description),
            }
          ]
        })

        //Loop remanings
        for (var i = 1; i < data.length; i++) {
          //Check if its in same date
          if (moment(dailyData[id].date).format('l') == moment(data[i].dt_txt).format('l')) {
            //Push timeslot data
            dailyData[id].timeSlot.push({
              time: moment(data[i].dt_txt).format('LT'),
              min: data[i].main.temp_min,
              max: data[i].main.temp_max,
              weather: this.convertUpperCase(data[i].weather[0].description),
            })

            slotNumber++;

            //Keep track of lowest and highest temperature 
            if (dailyData[id].timeSlot[slotNumber].min < minTemp) {
              minTemp = dailyData[id].timeSlot[slotNumber].min;
            }
            if (dailyData[id].timeSlot[slotNumber].max > maxTemp) {
              maxTemp = dailyData[id].timeSlot[slotNumber].max;
            }
          } else //New date 
          {
            //Update min and max 
            dailyData[id].min = minTemp;
            dailyData[id].max = maxTemp;

            //reset min and max variable 
            minTemp = 9999;
            maxTemp = 0;
            slotNumber = 0;

            //Push new data
            dailyData.push({
              id: id + 1,
              date: moment(data[i].dt_txt).format('l'),
              timeSlot: [
                {
                  time: moment(data[i].dt_txt).format('LT'),
                  min: data[i].main.temp_min,
                  max: data[i].main.temp_max,
                  weather: this.convertUpperCase(data[i].weather[0].description),
                }
              ]
            })
            id++;
          }
        }

        //Update final min and max temperature
        dailyData[id].min = dailyData[id].timeSlot[slotNumber].min;
        dailyData[id].max = dailyData[id].timeSlot[slotNumber].max;

        this.setState({
          dailyData: dailyData,
          titleDate: moment(dailyData[0].date).format('ddd, DD MMM YYYY h:mm a'),
          titleTemp: dailyData[0].timeSlot[0].min + ' °C',
          titleWeather: dailyData[0].timeSlot[0].weather,
        })
      })
  }

  renderModal = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.showModal}>
        <View style={styles.modalContainer}>
          <View style={styles.modal}>
            <FlatList
              data={this.state.detailData}
              keyExtractor={(item, index) => index.toString()}
              ItemSeparatorComponent={() => <View style={{ borderBottomWidth: 0.5 }}></View>}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={styles.flContainerColumn}
                >
                  <ThemeTextMedium title={item.time} style={styles.boldText} />
                  <ThemeTextMedium title={item.min + '°C - ' + item.max + '°C'} style={styles.normalText} />
                  <ThemeTextMedium title={item.weather} style={styles.lightText} />
                </TouchableOpacity>
              )}
            />
            <View style={styles.buttonContainer}>
              <ThemeButton title='Close' style={styles.button} onPress={() => this.setState({ showModal: false })} />
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderModal()}
        <View style={styles.topContainer}>
          <View style={styles.rowContainer}>
            <ThemeTextLarge title={this.state.titleDate} style={styles.boldText} />
            <ThemeTextExtraLarge title={this.state.titleTemp} />
            <ThemeTextLarge title={this.state.titleWeather} style={styles.lightText} />
          </View>
        </View>
        <View style={styles.bottomContainer}>
          <FlatList
            data={this.state.dailyData}
            keyExtractor={(item, index) => index.toString()}
            ItemSeparatorComponent={() => <View style={{ borderBottomWidth: 0.5 }}></View>}
            renderItem={({ item }) => (
              <TouchableOpacity
                style={styles.flContainer}
                onPress={() => this.showDetail(item.id)}
              >
                <View style={{ flex: 7 }}>
                  <ThemeTextMedium title={moment(item.date).format('DD MMM YYYY, ddd')} style={styles.boldText} />
                  <ThemeTextMedium title={item.min + '°C - ' + item.max + '°C'} style={styles.normalText} />
                  <ThemeTextMedium title={item.timeSlot[0].weather} style={styles.lightText} />
                </View>
                <View style={{ flex: 1 }}>
                  <Icon name="chevron-right" size={20} color={Color.themeRed} />
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  rowContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  topContainer: {
    flex: 1,
    backgroundColor: Color.white,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  bottomContainer: {
    flex: 3,
    backgroundColor: Color.white,
    paddingHorizontal: 10,
  },
  boldText: {
    fontWeight: 'bold',
    marginVertical: 1,
  },
  normalText: {
    fontWeight: 'normal',
    marginVertical: 1,
  },
  lightText: {
    color: Color.darkGrey,
    marginVertical: 1,
  },
  //FlatList
  flContainer: {
    flexDirection: 'row',
    marginVertical: 10,
    alignItems: 'center',
  },
  flContainerColumn: {
    flexDirection: 'column',
    marginVertical: 10,
  },
  //Modal
  modalContainer: {
    flex: 1,
    alignItems: 'center',
    marginVertical:'10%',
    justifyContent: 'center',
  },
  modal: {
    width: '80%',
    paddingHorizontal: 10,
    backgroundColor: Color.white,
    borderWidth: 1,
    borderRadius: 5,
  },
  button: {
    flex: 1,
    marginVertical: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginHorizontal: '10%',
  },
});
