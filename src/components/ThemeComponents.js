import React, { Component } from 'react';
import { TouchableOpacity, Text, StyleSheet, Dimensions, ImageBackground, TextInput, Modal, Image, View, TouchableWithoutFeedback } from 'react-native';
import Color from '../utils/Color';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

//For debugging 
const layoutLining = 0;

export const ThemeTextSmall = (props) => {
    const { style = {} } = props;

    return (
        <Text style={[styles.textSmall, style]}>{props.title}</Text>
    );
};

export const ThemeTextMedium = (props) => {
    const { style = {} } = props;

    return (
        <Text style={[styles.textMedium, style]}>{props.title}</Text>
    );
};

export const ThemeTextLarge = (props) => {
    const { style = {} } = props;

    return (
        <Text style={[styles.textLarge, style]}>{props.title}</Text>
    );
};

export const ThemeTextExtraLarge = (props) => {
    const { style = {} } = props;

    return (
        <Text style={[styles.textExtraLarge, style]}>{props.title}</Text>
    );
};

export const ThemeButton = (props) => {
    const { style = {}, textStyle = {}, onPress, disabled } = props;

    return (
        <TouchableOpacity onPress={onPress} style={[styles.button, style]} disabled={disabled}>
            <Text style={[styles.buttonText, textStyle]}>{props.title}</Text>
        </TouchableOpacity>
    );
};

export const ThemeTextbox = (props) => {
    const {style = {}, textStyle = {}, onPress, disabled } = props;

    return (
        <TextInput
            editable
            style={[styles.textbox, style]}
            {...props}
            value={props.title}
            maxLength={25}
        />
    );
};

const styles = StyleSheet.create({
    textSmall: {
        color: Color.black,
        fontSize: 12,
        fontFamily: "Helvetica Neue",
    },
    textMedium: {
        color: Color.black,
        fontSize: 14,
        fontFamily: "Helvetica Neue",
    },
    textLarge: {
        color: Color.black,
        fontSize: 26,
        fontFamily: "Helvetica Neue",
    },
    textExtraLarge: {
        color: Color.black,
        fontSize: 38,
        fontFamily: "Helvetica Neue",
    },
    button: {
        backgroundColor: Color.themeRed,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        height: height * 0.04,
    },
    buttonText: {
        color: Color.white,
        fontSize: 14,
        fontWeight: 'bold',
        fontFamily: "Helvetica Neue",
    },
    textbox:{
        fontSize: 26,
    }
});