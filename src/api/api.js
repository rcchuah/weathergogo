
//'http://api.openweathermap.org/data/2.5/forecast?q=London&appid=878ea5b4ea4034799561b3ac5932aad3';
const mainURL = 'http://api.openweathermap.org/data/2.5/forecast';
const appId = '878ea5b4ea4034799561b3ac5932aad3';

export const GetWeatherByCity = async (city)  => {
    //const URL = mainURL + '?q=' + city + '&cnt=30&appid=' + appId;
    const URL = mainURL + '?q=' + city + '&units=metric&appid=' + appId;
    console.log(URL);
    // Call API 
    return await fetch(URL,
        {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
        .then((response) => response.json())
        .catch((error) => {
            console.error(error);
        });
}
