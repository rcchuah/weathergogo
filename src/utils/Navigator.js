import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../screens/Home';
import Welcome from '../screens/Welcome';
import Color from './Color';

const Stack = createStackNavigator();

export function Navigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{
        headerTitleStyle:{color:Color.white},
        headerStyle: {backgroundColor: Color.themeRed },
        headerBackTitleStyle:{color:Color.white},
        headerTintColor:  Color.white,

      }}>
        <Stack.Screen
          name="Welcome"
          component={Welcome}
        />
        <Stack.Screen
          name="Home"
          component={Home}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}